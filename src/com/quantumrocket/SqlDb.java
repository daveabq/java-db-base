/*
 * Copyright 2005-2018 QuantumRocket. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 *
 * General purpose database utility functions.
 */

package com.quantumrocket;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class SqlDb {

    public static void main(String[] args) {

        String url = "jdbc:postgresql://localhost/testdb";
        String user = "me";
        String password = "my-password";

        try {

            SqlDb db = new SqlDb(url, user, password);

            ArrayList<HashMap<String, Object>> rows = null;


            // select_all, no where clause

            ArrayList<String> cols = new ArrayList<>();
            cols.add("user_id");
            cols.add("email");
            cols.add("full_name");

            rows = db.select_all("users", null, null, cols);
            dump_rows("select_all, no where clause", rows);


            // select_all, like

            HashMap<String, Object> where = new HashMap<String, Object>();
            where.put("user_id", "30%");

            rows = db.select_all_like("users", where, null, cols);
            dump_rows("select_all, like", rows);


            // select_all with where clause

            where = new HashMap<String, Object>();
            where.put("email", "dave@crowdspeak.com");

            rows = db.select_all("users", where, null, cols);
            dump_rows("select_all with where clause", rows);

            // select_single with where clause

            where = new HashMap<String, Object>();
            where.put("email", "dave@crowdspeak.com");

            HashMap<String, Object> row = db.select_single("users", where, null, cols);
            dump_row("select_single with where clause", row);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    Connection conn = null;

    SqlDb(String url, String user, String password) throws SQLException {

        conn = DriverManager.getConnection(url, user, password);

    }

    private static void dump_rows(String header, ArrayList<HashMap<String, Object>> rows) {

        if (header != null) {
            System.out.println("\n-- " + header + " --\n");
        }

        if (rows != null) {

            for (HashMap<String, Object> row : rows) {

                for (String col : row.keySet()) {
                    System.out.println(col + " -> " + row.get(col));
                }

                System.out.println("");
            }

        } else {
            System.out.println("[no rows]");
        }

    }

    private static void dump_row(String header, HashMap<String, Object> row) {

        if (header != null) {
            System.out.println("\n-- " + header + " --\n");
        }

        if (row != null) {

            for (String col : row.keySet()) {
                System.out.println(col + " -> " + row.get(col));
            }

        } else {
            System.out.println("[no row]");
        }

    }

    // Create a list of maps, each map containing the column names and values for a single row.
    private ArrayList<HashMap<String, Object>> getData(ResultSet rs, ArrayList<String> columns) throws SQLException {

        ArrayList<HashMap<String, Object>> map = new ArrayList<>();

        while (rs.next()) {

            HashMap<String, Object> hash = new HashMap<>();

            for (String col : columns) {
                hash.put(col, rs.getString(col));
            }

            map.add(hash);

        }

        return map;

    }

    public HashMap<String, Object> select_single(String table, HashMap<String, Object> where, ArrayList<String> order_by, ArrayList<String> cols) throws SQLException {

        ArrayList<HashMap<String, Object>> rows = select(table, where, order_by, cols, false);

        if (rows != null) {
            return rows.get(0);
        } else {
            return null;
        }

    }

    public HashMap<String, Object> select_single_like(String table, HashMap<String, Object> where, ArrayList<String> order_by, ArrayList<String> cols) throws SQLException {

        ArrayList<HashMap<String, Object>> rows = select(table, where, order_by, cols, true);

        if (rows != null) {
            return rows.get(0);
        } else {
            return null;
        }

    }

    public ArrayList<HashMap<String, Object>> select_all(String table, HashMap<String, Object> where, ArrayList<String> order_by, ArrayList<String> cols) throws SQLException {

        ArrayList<HashMap<String, Object>> rows = select(table, where, order_by, cols, false);

        return rows;

    }

    public ArrayList<HashMap<String, Object>> select_all_like(String table, HashMap<String, Object> where, ArrayList<String> order_by, ArrayList<String> cols) throws SQLException {

        ArrayList<HashMap<String, Object>> rows = select(table, where, order_by, cols, true);

        return rows;

    }

    /**
     * Execute a SELECT statement, based on the supplied parameters.
     *
     * Given the call:
     *
     *     select_all('widgets', {'user_id':'01CS2P9P684BAA6NCHDDN4D704'}, ['widget_name'],
     *                ['widget_id', 'widget_name', 'description'], False)
     *
     * the following SQL will be created:
     *
     *     SELECT widget_id, widget_name, description FROM widgets WHERE user_id = %s ORDER BY widget_name
     *
     * For this call, we have more elements in both the 'where' and 'order_by' params:
     *
     *     select('widgets', {'user_id':'01CS2P9P684BAA6NCHDDN4D704, 'user_email':'a@a.a'},
     *            ['widget_name', 'description'], ['widget_id', 'widget_name', 'description'], False)
     *
     * the following SQL will be created:
     *
     *     SELECT widget_id, widget_name, description FROM widgets WHERE user_id = %s AND user_email = %s
     *         ORDER BY widget_name, description
     *
     * In both of the examples above, the where clause placeholders (%s) will be filled in with the appropriate
     * right-side values of the 'where' parameter. So, the final SQL executed in the first example will be:
     *
     *     SELECT widget_id, widget_name, description FROM widgets WHERE user_id = '01CS2P9P684BAA6NCHDDN4D704'
     *         ORDER BY widget_name
     *
     * and the final SQL executed in the second example will be:
     *
     *     SELECT widget_id, widget_name, description FROM widgets WHERE user_id = '01CS2P9P684BAA6NCHDDN4D704'
     *         AND user_email = 'a@a.a' ORDER BY widget_name, description
     */
    public ArrayList<HashMap<String, Object>> select(String table, HashMap<String, Object> where, ArrayList<String> order_by, ArrayList<String> cols, boolean like) throws SQLException {

        try {

            StringBuilder sb = new StringBuilder("SELECT ");

            int col_count = cols.size();
            int element = 0;

            for (String col_name : cols) {

                sb.append(col_name);

                if (element++ < (col_count - 1)) {
                    sb.append(", ");
                }

            }

            sb.append(" FROM ").append(table);

            ArrayList<Object> where_values = new ArrayList<>();

            if (where != null) {

                sb.append(" WHERE ");

                for (String col_name : where.keySet()) {

                    if (where_values.size() > 0) {
                        sb.append(" AND ");
                    }

                    if (like) {
                        sb.append(col_name).append(" LIKE ?");
                    } else {
                        sb.append(col_name).append(" = ?");
                    }

                    where_values.add(where.get(col_name));

                }

            }

            if (order_by != null) {

                sb.append(" ORDER BY ");
                element = 0;
                col_count = order_by.size();

                for (String col_name : order_by) {

                    sb.append(col_name);

                    if (element < (col_count - 1)) {
                        sb.append(", ");
                    }
                }

            }

            PreparedStatement ps = this.conn.prepareStatement(sb.toString());

            if (where != null) {

                int i = 1;

                for (Object value : where_values) {
                    ps.setObject(i++, value);
                }


            }

            ResultSet rs = ps.executeQuery();

            ArrayList<HashMap<String, Object>> data = getData(rs, cols);

            return data;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;

    }


    /**
     * Execute an INSERT statement, based on the supplied parameters.
     *
     * Given the call:
     *
     * insert('widgets', {'widget_id': '12345', 
     *        'widget_name': 'fluffy the pink bunny', 
     *        'description': 'super-mega-ultra bunny fluffiness'})
     *
     * the following SQL will be created:
     *
     * INSERT INTO widgets (widget_id, widget_name, description) VALUES
     *                     ('1235', fluffy the pink bunny',
     *                     'super-mega-ultra bunny fluffiness')
     */
    public void insert(String table, HashMap<String, Object> cols_and_values) throws SQLException {

        try {

            StringBuilder sb = new StringBuilder("INSERT INTO ").append(table).append(" (");

            int col_count = cols_and_values.size();
            int element = 0;
            ArrayList<Object> col_values = new ArrayList<>();

            for (String col_name : cols_and_values.keySet()) {

                sb.append(col_name);

                if (element++ < col_count - 1) {
                    sb.append(", ");
                }

                col_values.add(cols_and_values.get(col_name));

            }

            sb.append(") VALUES (");
            element = 0;

            for (String col_name : cols_and_values.keySet()) {

                sb.append("?");

                if (element++ < col_count - 1) {
                    sb.append(", ");
                }

            }

            sb.append(")");

            PreparedStatement ps = this.conn.prepareStatement(sb.toString());
            int i = 1;

            for (Object value : col_values) {
                ps.setObject(i++, value);
            }

            ResultSet rs = ps.executeQuery();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
